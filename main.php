<div id="search-app" class="col-xs-12">
    <form class="form-horizontal" method="get" action="/blog/company">
        <input type="search" name="val" class="form-control" placeholder="Введите название компании"
               value="<?= isset($_GET['val']) ? $_GET['val'] : '' ?>">
        <input type="submit" value="Поиск">
    </form>

    <?php
    if (isset($_GET['val'])) :
        include(plugin_dir_path(__FILE__).'template-parts/companies.php');
    endif;
    $companyId = get_query_var('company_id');
    $companyType = get_query_var('type');
    if (isset($companyId)) : ?>
        <?php $company = searchCompanyDataById($companyId);
        if ($company) : ?>
            <h2 class="company-title title js-company-name"
                data-id="<?= $companyId ?>"><?= $company->company_name ?></h2>
            <h4 class="title"> Номер
                компании: <?= $company->company_number ?></h4>
            <?php if (isset($_GET['subscribe'])):
                include(plugin_dir_path(__FILE__)
                    .'template-parts/subscribe.php');
            elseif (isset($_GET['unsubscribe'])):
                include(plugin_dir_path(__FILE__)
                    .'template-parts/unsubscribe.php');
            else: ?>
                <?php if (is_user_logged_in()
                    && checkUserSubscribe($_GET['company_id'])
                ): ?>
                    <div><a href='<?= $_SERVER['REQUEST_URI']
                        .'?unsubscribe=1' ?>' class='button '>Отписаться</a></div>
                <?php else: ?>
                    <div><a href='<?= $_SERVER['REQUEST_URI']
                        .'?subscribe=1' ?>' class='button'>Подписаться</a>
                    </div>
                    <h4 class="title">Если подписаться, то при любом изменении в
                        компании, приедет уведомление на email.
                    </h4>
                <?php endif; ?>
                <ul class="nav nav-tabs">
                    <?= showMenu($company, $companyType) ?>
                </ul>
                <div class="tab-content" id="ch-plugin-main-block">
                    <?php switch ($companyType) {
                        case 'filing-history':
                            $company_history
                                = searchCompanyDataById(
                                $companyId,
                                'filing-history'
                            );
                            include(plugin_dir_path(__FILE__)
                                .'template-parts/company-documents.php');
                            break;
                        case 'officers':
                            $company_officers
                                = searchCompanyDataById(
                                $_GET['company_id'],
                                'officers'
                            );
                            $company_sc_officers
                                = searchCompanyDataById(
                                $_GET['company_id'],
                                'persons-with-significant-control'
                            );
                            include(plugin_dir_path(__FILE__)
                                .'template-parts/company-officers.php');
                            break;
                        case 'officers':
                            $company_officers
                                = searchCompanyDataById(
                                $_GET['company_id'],
                                'officers'
                            );
                            $company_sc_officers
                                = searchCompanyDataById(
                                $_GET['company_id'],
                                'persons-with-significant-control'
                            );
                            include(plugin_dir_path(__FILE__)
                                .'template-parts/company-officers.php');
                            break;
                        case 'persons-with-significant-control-statements':
                            $company_sc_officers
                                = searchCompanyDataById(
                                $_GET['company_id'],
                                'persons-with-significant-control'
                            );
                            $statements
                                = searchCompanyDataById(
                                $_GET['company_id'],
                                'persons-with-significant-control-statements'
                            );
                            include(plugin_dir_path(__FILE__)
                                .'template-parts/company-officers.php');
                            break;
                        case 'charges':
                            if (isset($_GET['chargeSlug'])) {
                                $chargeData = searchCompanyDataById(
                                    $_GET['company_id'],
                                    $companyType.'/'.$_GET['chargeSlug']
                                );
                                include(plugin_dir_path(__FILE__).'template-parts/company-charges-details.php');
                            } else {
                                $chargeData = searchCompanyDataById(
                                    $_GET['company_id'],
                                    $companyType
                                );
                                include(plugin_dir_path(__FILE__).'template-parts/company-charges.php');
                            }
                            break;
                        case 'view-all':
                            include(plugin_dir_path(__FILE__)
                                .'template-parts/view-all.php');
                            break;
                        default:
                            include(plugin_dir_path(__FILE__)
                                .'template-parts/company-info.php');
                            break;
                    } ?>
                    <div id="robinzon"><a href=' #' OnClick="history.back();" class='button'>Вернуться обратно</a>
                    </div>

                </div>
            <?php endif;
        endif;
    endif;
    ?>

</div>