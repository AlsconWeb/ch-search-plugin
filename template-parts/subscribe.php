<?php if (is_user_logged_in()): ?>
    <p>Адрес электронной почты, который мы будем использовать для отправки уведомлений о деятельности компаний - <?= wp_get_current_user()->user_email ?> </p>
<?php endif; ?>

<h4 class="title"> Вы уверены, что хотите подписаться на обновления этой компании?</h4>
<h4 class="title">Вы можете отменить подписку на эту компанию в любое время.</h4>

<div><a href="#" class='button js-subscribe-button'>Подписаться</a></div>
<div><a href="#" class='button' onclick="window.history.go(-1); return false;">Вернуться назад</a></div>