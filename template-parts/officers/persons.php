<div class="active" id="block-people">
    <h3 class="title">Люди </h3>
    <?php if ($company_officers->total_results) : ?>

        <div class="col-xs-12">
            <form class="form-horizontal pages" method="get"
                  action="#">
                <label for="show-current">Действительные должностные
                    лица</label>
                <input type="checkbox" id="show-current"/>
            </form>
        </div>

        <h3 class="title"><?= $company_officers->total_results ?>
            найдено</h3>
        <h2 class="title"><span class="js-officers-total"></span>
            должностных лиц</h2>
        <h2 class="title"><span class="js-officers-resigned"></span>
            в отставке</h2>
        <h2 class="title" style="display: none"><span
                    class="js-officers-current"></span>
            действительных должностных лиц</h2>
        <?php foreach ($company_officers->items as $item) : ?>
            <div class="officer js-officer-item js-officer<?= !isset($item->resigned_on)
                ? '-active' : '' ?>">
                <h2 class="title"><?= $item->name ?>
                    <?php if (!isset($item->resigned_on)) : ?>
                    <span class="status active">ACTIVE</span></h2>
                <?php else: ?>
                    <span class="status resigned">RESIGNED</span></h2>
                <?php endif; ?>
                <h4 class='title'>Почтовый адрес</h4>
                <?php if (isset($item->address)) : ?>
                    <p class="title">
                        <?= isset($item->address->premises)
                            ? $item->address->premises . ', '
                            : '' ?>
                        <?= isset($item->address->address_line_1)
                            ? $item->address->address_line_1 . ', '
                            : '' ?>
                        <?= isset($item->address->address_line_2)
                            ? $item->address->address_line_2 . ', '
                            : '' ?>
                        <?= isset($item->address->locality)
                            ? $item->address->locality . ', '
                            : '' ?>
                        <?= isset($item->address->region)
                            ? $item->address->region . ', ' : '' ?>
                        <?= isset($item->address->country)
                            ? $item->address->country . ', ' : '' ?>
                        <?= isset($item->address->postal_code)
                            ? $item->address->postal_code : '' ?>
                    </p>
                <?php endif; ?>

                <ul class='description'>

                    <?php if (isset($item->officer_role)) : ?>
                        <li>
                            <h4 class="title">Должность</h4>
                            <p class="title"> <?= str_replace('-',
                                    ' ', $item->officer_role) ?></p>
                        </li>
                    <?php endif; ?>

                    <?php if (isset($item->notified_on)) : ?>
                        <li>
                            <h4 class="title">Зарегистрирован</h4>
                            <p class="title"> <?= formattingDate($item->notified_on) ?></p>
                        </li>
                    <?php endif; ?>

                    <?php if (isset($item->date_of_birth)) : ?>
                        <li>
                            <h4 class="title">Дата рождения</h4>
                            <p class="title"> <?= $item->date_of_birth->month
                                . '.'
                                . $item->date_of_birth->year ?></p>
                        </li>
                    <?php endif; ?>

                    <?php if (isset($item->appointed_on)) : ?>
                        <li>
                            <h4 class="title">Назначен</h4>
                            <p class="title"> <?= formattingDate($item->appointed_on) ?></p>
                        </li>
                    <?php endif; ?>

                    <?php if (isset($item->resigned_on)) : ?>
                        <li>
                            <h4 class="title">Подал в отставку</h4>
                            <p class="title"> <?= formattingDate($item->resigned_on) ?></p>
                        </li>
                    <?php endif; ?>

                    <?php if (isset($item->identification)) : ?>

                        <?php if (isset($item->identification->identification_type)) : ?>
                            <?php if ($item->identification->identification_type
                                == 'non-eea'
                            ) : ?>
                                <h5 class="title">Зарегистрирован не
                                    в Европейской
                                    экономической
                                    зоне</h5>
                                <br>
                            <?php endif; ?>
                        <?php endif; ?>

                        <?php if (isset($item->identification->legal_authority)) : ?>
                            <li>
                                <h4 class="title">Регулируемый
                                    правовыми нормами</h4>
                                <p class="title"> <?= $item->identification->legal_authority ?></p>
                            </li>
                        <?php endif; ?>

                        <?php if (isset($item->identification->legal_form)) : ?>
                            <li>
                                <h4 class="title">Юридическая
                                    форма</h4>
                                <p class="title"> <?= $item->identification->legal_form ?></p>
                            </li>
                        <?php endif; ?>

                        <?php if (isset($item->identification->place_registered)) : ?>
                            <li>
                                <h4 class="title">Место
                                    регистрации</h4>
                                <p class="title"> <?= $item->identification->place_registered ?></p>
                            </li>
                        <?php endif; ?>

                        <?php if (isset($item->identification->registration_number)) : ?>
                            <li>
                                <h4 class="title">Регистрационный
                                    номер</h4>
                                <p class="title"> <?= $item->identification->registration_number ?></p>
                            </li>
                        <?php endif; ?>

                    <?php endif; ?>

                    <?php if (isset($item->nationality)) : ?>
                        <li>
                            <h4 class="title">Национальность</h4>
                            <p class="title"> <?= $item->nationality ?></p>
                        </li>
                    <?php endif; ?>

                    <?php if (isset($item->country_of_residence)) : ?>
                        <li>
                            <h4 class="title">Страна проживания</h4>
                            <p class="title"> <?= $item->country_of_residence ?></p>
                        </li>
                    <?php endif; ?>

                    <?php if (isset($item->occupation)) : ?>
                        <li>
                            <h4 class="title">Профессия</h4>
                            <p class="title"> <?= $item->occupation ?></p>
                        </li>
                    <?php endif; ?>

                    <?php if (isset($item->natures_of_control)) : ?>
                        <li>
                            <h4 class="title">Характер
                                управления</h4>
                            <?php foreach ($item->natures_of_control as $value) ?>
                            <p class="people-nc title"> <?= str_replace('-', ' ', $value) ?></p>
                        </li>
                    <?php endif; ?>
                </ul>

            </div>
        <?php endforeach; ?>
    <?php else: ?>
        <h2 id="not-items-found">Отсутствует информация по Вашему
            запросу</h2>
    <?php endif; ?>
</div>