<div class="active" id="block-people-ch">
    <h3 class="title">Люди со значительным контролем </h3>
    <?php if (isset($company_sc_officers->total_results)
        || isset($statements->total_results)
    ) : ?>

        <?php if (isset($company_sc_officers)) : ?>
            <?php if (isset($company_sc_officers->total_results)): ?>
                <h3 class="title"><?= $company_sc_officers->total_results ?>
                    найдено</h3>
            <?php endif ?>
            <h2 class="title"><?= $company_sc_officers->active_count
                    ? $company_sc_officers->active_count : 0 ?>
                активных лиц контролирующих компанию</h2>
            <h2 class="title"><?= $statements->active_count
                    ? $statements->active_count : 0 ?> активных
                состояний </h2>
            <?php if (isset($statements->total_results)) : ?>

                <?php foreach ($statements->items as $key => $item):
                    ?>

                    <?php if (!$item->ceased_on) :
                    unset($statements->items[$key]);
                    ?>
                    <div class="officer">
                        <h2 class="title">Состояние<span
                                class="status active">АКТИВНО</span>
                        </h2>
                        <p class="title">
                            Партнерстово знает или имеет основания
                            пологать, что нет лица контролирующего
                            компанию.
                        </p>

                        <?php if ($item->statement
                            == 'no-individual-or-entity-with-signficant-control'
                        ): ?>
                            <p class="title">
                                В компании нет контролирующего лица
                                (PSC) или значимого
                                юридического лица (RLE).
                            </p>
                        <?php endif; ?>
                        <ul class='description'>
                            <?php if (isset($item->notified_on)) : ?>
                                <li>
                                    <h4 class="title">Сообщено </h4>
                                    <p class="title"> <?= formattingDate($item->notified_on); ?></p>
                                </li>
                            <?php endif; ?>
                            <?php if (isset($item->ceased_on)) : ?>
                                <li>
                                    <h4 class="title">Прекращено
                                        с</h4>
                                    <p class="title"> <?= formattingDate($item->ceased_on); ?></p>
                                </li>
                            <?php endif; ?>
                        </ul>
                    </div>
                <?php endif; ?>
                <?php endforeach; ?>
            <?php endif; ?>
            <?php if (isset($company_sc_officers->items)): ?>
                <?php foreach ($company_sc_officers->items as $item)
                    : ?>
                    <div class="officer">
                        <h2 class="title"><?= $item->name ?>
                            <?php if (!$item->ceased_on) : ?>
                            <span class="status active">АКТИВНО</span>
                        </h2>
                        <?php else: ?>
                            <span class="status resigned">ПРИОСТАНОВЛЕНО</span></h2>
                        <?php endif; ?>
                        <h4 class='title'>Почтовый адрес</h4>
                        <?php if (isset($item->address)) : ?>
                            <p class="title">
                                <?= $item->address->premises
                                    ? $item->address->premises
                                    . ', ' : '' ?>
                                <?= $item->address->address_line_1
                                    ? $item->address->address_line_1
                                    . ', ' : '' ?>
                                <?= $item->address->address_line_2
                                    ? $item->address->address_line_2
                                    . ', ' : '' ?>
                                <?= $item->address->locality
                                    ? $item->address->locality
                                    . ', ' : '' ?>
                                <?= $item->address->region
                                    ? $item->address->region . ', '
                                    : '' ?>
                                <?= $item->address->country
                                    ? $item->address->country . ', '
                                    : '' ?>
                                <?= $item->address->postal_code
                                    ? $item->address->postal_code
                                    : '' ?>
                            </p>
                        <?php endif; ?>

                        <ul class='description'>

                            <?php if (isset($item->officer_role)) : ?>
                                <li>
                                    <h4 class="title">Должность</h4>
                                    <p class="title"> <?= str_replace('-',
                                            ' ',
                                            $item->officer_role) ?></p>
                                </li>
                            <?php endif; ?>

                            <?php if (isset($item->notified_on)) : ?>
                                <li>
                                    <h4 class="title">
                                        Зарегистрирован</h4>
                                    <p class="title"> <?= formattingDate($item->notified_on) ?></p>
                                </li>
                            <?php endif; ?>

                            <?php if (isset($item->date_of_birth)) : ?>
                                <li>
                                    <h4 class="title">Дата
                                        рождения</h4>
                                    <p class="title"> <?= $item->date_of_birth->month
                                        . '.'
                                        . $item->date_of_birth->year ?></p>
                                </li>
                            <?php endif; ?>

                            <?php if (isset($item->appointed_on)) : ?>
                                <li>
                                    <h4 class="title">Назначен</h4>
                                    <p class="title"> <?= formattingDate($item->appointed_on) ?></p>
                                </li>
                            <?php endif; ?>

                            <?php if (isset($item->ceased_on)) : ?>
                                <li>
                                    <h4 class="title">Прекращено
                                        с</h4>
                                    <p class="title"> <?= formattingDate($item->ceased_on) ?></p>
                                </li>
                            <?php endif; ?>

                            <?php if (isset($item->identification)) : ?>

                                <?php if (isset($item->identification->identification_type)) : ?>
                                    <?php if ($item->identification->identification_type
                                        == 'non-eea'
                                    ) : ?>
                                        <h5 class="title">
                                            Зарегистрирован не в
                                            Европейской
                                            экономической
                                            зоне</h5>
                                        <br>
                                    <?php endif; ?>
                                <?php endif; ?>

                                <?php if (isset($item->identification->legal_authority)) : ?>
                                    <li>
                                        <h4 class="title">
                                            Регулируемый правовыми
                                            нормами</h4>
                                        <p class="title"> <?= $item->identification->legal_authority ?></p>
                                    </li>
                                <?php endif; ?>

                                <?php if (isset($item->identification->legal_form)) : ?>
                                    <li>
                                        <h4 class="title">
                                            Юридическая форма</h4>
                                        <p class="title"> <?= $item->identification->legal_form ?></p>
                                    </li>
                                <?php endif; ?>

                                <?php if (isset($item->identification->place_registered)) : ?>
                                    <li>
                                        <h4 class="title">Место
                                            регистрации</h4>
                                        <p class="title"> <?= $item->identification->place_registered ?></p>
                                    </li>
                                <?php endif; ?>

                                <?php if (isset($item->identification->registration_number)) : ?>
                                    <li>
                                        <h4 class="title">
                                            Регистрационный
                                            номер</h4>
                                        <p class="title"> <?= $item->identification->registration_number ?></p>
                                    </li>
                                <?php endif; ?>

                            <?php endif; ?>

                            <?php if (isset($item->nationality)) : ?>
                                <li>
                                    <h4 class="title">
                                        Национальность</h4>
                                    <p class="title"> <?= $item->nationality ?></p>
                                </li>
                            <?php endif; ?>

                            <?php if (isset($item->country_of_residence)) : ?>
                                <li>
                                    <h4 class="title">Страна
                                        проживания</h4>
                                    <p class="title"> <?= $item->country_of_residence ?></p>
                                </li>
                            <?php endif; ?>

                            <?php if (isset($item->occupation)) : ?>
                                <li>
                                    <h4 class="title">Профессия</h4>
                                    <p class="title"> <?= $item->occupation ?></p>
                                </li>
                            <?php endif; ?>

                            <?php if (isset($item->natures_of_control)) :?>
                                <li>
                                    <h4 class="title">Характер
                                        управления</h4>
                                    <?php foreach ($item->natures_of_control
                                        as $value): ?>
                                    <p class="people-nc title"> <?= str_replace('-',
                                            ' ', $value) ?></p>
                                    <?php endforeach; ?>
                                </li>
                            <?php endif; ?>
                        </ul>

                    </div>
                <?php endforeach; ?>
            <?php endif; ?>
        <?php endif; ?>
        <?php if (isset($statements->total_results)) : ?>

            <?php foreach ($statements->items as $item) : ?>
                <div class="officer">
                    <?php if (isset($item->ceased_on)) : ?>
                        <h2 class="title">Состояние<span
                                class="status resigned">ПРЕКРАЩЕНО</span>
                        </h2>
                    <?php else : ?>
                        <h2 class="title">Состояние<span
                                class="status active">АКТИВНО</span>
                        </h2>
                        <p class="title">
                            Партнерстово знает или имеет основания
                            пологать, что нет лица контролирующего
                            компанию.
                        </p>
                    <?php endif; ?>

                    <?php if ($item->statement
                        == 'no-individual-or-entity-with-signficant-control'
                    ): ?>
                        <p class="title">
                            В компании нет контролирующего лица
                            (PSC) или значимого
                            юридического лица (RLE).
                        </p>

                    <?php elseif (isset($item->ceased_on)
                        && $item->statement
                        == 'steps-to-find-psc-not-yet-completed-partnership'
                    ): ?>
                        <p class="title">
                            Партнерство еще не определилось, есть ли
                            кто-либо, кто является
                            контролирующим лицом (PSC) или значимым
                            юридическим лицом (RLE)
                        </p>
                    <?php endif; ?>
                    <ul class='description'>
                        <?php if (isset($item->notified_on)) : ?>
                            <li>
                                <h4 class="title">Сообщено </h4>
                                <p class="title"> <?= formattingDate($item->notified_on); ?></p>
                            </li>
                        <?php endif; ?>
                        <?php if (isset($item->ceased_on)) : ?>
                            <li>
                                <h4 class="title">Прекращено с</h4>
                                <p class="title"> <?= formattingDate($item->ceased_on); ?></p>
                            </li>
                        <?php endif; ?>
                    </ul>
                </div>
            <?php endforeach; ?>
        <?php endif; ?>
    <?php else: ?>
        <h2 id="not-items-found">Отсутствует информация по Вашему
            запросу</h2>
    <?php endif; ?>
</div>