<div class="active" id="block">
    <?php if (isset($chargeData->charge_code)): ?>
        <h1 class="title">Charge code <?= $chargeData->charge_code ?></h1>
    <?php else: ?>
        <h1 class="title"><?= $chargeData->classification->description ?></h1>
    <?php endif; ?>
    <h2 class="title">Создано:</h2>
    <p class="title"><?= formattingDate($chargeData->created_on) ?></p>
    <h2 class="title">Доставлено:</h2>
    <p class="title"><?= formattingDate($chargeData->delivered_on) ?></p>
    <h2 class="title">Статус:</h2>
    <p class="title"><?= translateChargeStatus($chargeData->status)
        .' '
        .(isset($chargeData->satisfied_on) ? formattingDate($chargeData->satisfied_on) : '') ?></p>
    <?php $chargeRegistrationDocumentLink = prepareChargeRegistrationDocumentLink($chargeData->transactions); ?>
    <?php if (!empty($chargeRegistrationDocumentLink)): ?>
        <h2 class="title">Регистация обременения:</h2>
        <p class="title">
            <a href="https://beta.companieshouse.gov.uk<?= $chargeRegistrationDocumentLink ?>/document?format=pdf&download=0"
               target="_blank" download>открыть PDF</a>
        </p>
    <?php endif; ?>

    <?php if (isset($chargeData->persons_entitled) && is_array($chargeData->persons_entitled)): ?>
        <h2 class="title">Обременитель: </h2>
        <?php foreach ($chargeData->persons_entitled as $person): ?>
            <p class="title"><?= $person->name ?></p>
        <?php endforeach; ?>
    <?php endif; ?>
    <?php if (isset($chargeData->secured_details)): ?>
        <h2 class="title"><?= translateAmountSecuredType($chargeData->secured_details->type) ?></h2>
        <p class="title"><?= $chargeData->secured_details->description ?></p>
    <?php endif; ?>
    <?php if (isset($chargeData->particulars)): ?>
        <h2 class="title"><?= translateParticularsType($chargeData->particulars->type) ?></h2>
        <p class="title"><?= $chargeData->particulars->description ?></p>
    <?php endif; ?>
    <?php if (isset($chargeData->transactions) && is_array($chargeData->transactions) && count($chargeData->transactions) > 1): ?>
        <div class="tr">
            <div class="th">
                <p><strong>Тип</strong></p>
            </div>
            <div class="th">
                <p><strong>Доставлено</strong></p>
            </div>
            <div class="th">
                <p><strong>Просмотреть</strong></p>
            </div>
        </div>
        <?php foreach ($chargeData->transactions as $transaction): ?>
            <?php $search = 'create-charge'; ?>
            <?php if (!preg_match("/{$search}/i", $transaction->filing_type)): ?>
                <div class="tr">
                    <div class="td">
                        <p><?= prepareTitle($transaction->filing_type) ?></p>
                    </div>
                    <div class="td">
                        <p><?= formattingDate($transaction->delivered_on) ?></p>
                    </div>
                    <div class="td">
                        <p class='download-pdf-now'>
                            <a href="https://beta.companieshouse.gov.uk<?= $transaction->links->filing ?>/document?format=pdf&download=0"
                               target="_blank" download>открыть PDF</a>
                        </p>
                    </div>
                </div>
            <?php endif; ?>
        <?php endforeach; ?>
    <?php endif; ?>
</div>