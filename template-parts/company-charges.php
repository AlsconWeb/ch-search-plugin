<div class="active" id="block">
    <p><b>Фильтр обременений</b></p>
    <div class="rememberme">
        <label for="filter-charges-input">
            <input name="filter-charges-input" id="filter-charges-input" type="checkbox" value="1"> Не исполнено /
            частично выполнено
        </label>
    </div>

    <h4 class='title'>Зарегистрировано обременений: <?= $chargeData->total_count ?></h4>
    <p class="title">Не исполнено: <?= $chargeData->total_count - $chargeData->satisfied_count - $chargeData->part_satisfied_count ?></p>
    <p class="title">Выполнено: <?= $chargeData->satisfied_count ?></p>
    <p class="title">Частично выполнено: <?= $chargeData->part_satisfied_count ?></p>
    <?php foreach ($chargeData->items as $item): ?>
        <div class="officer js-charge-item" data-status="<?= $item->status ?>">
            <?php if (isset($item->charge_code)): ?>
                <a href="/blog<?= prepareChargeDetailUrl($item->links->self) ?>">
                    <h1 class="title">Charge code <?= $item->charge_code ?></h1>
                </a>
            <?php else: ?>
                <a href="/blog<?= prepareChargeDetailUrl($item->links->self) ?>">
                    <h1 class="title"><?= $item->classification->description ?></h1>
                </a>
            <?php endif; ?>
            <h2 class="title">Создано:</h2>
            <p class="title"><?= formattingDate($item->created_on) ?></p>
            <h2 class="title">Доставлено:</h2>
            <p class="title"><?= formattingDate($item->delivered_on) ?></p>
            <h2 class="title">Статус:</h2>
            <p class="title"><?= translateChargeStatus($item->status) ?></p>
            <?php if (isset($item->persons_entitled) && is_array($item->persons_entitled)): ?>
                <h2 class="title">Обременитель: </h2>
                <?php foreach ($item->persons_entitled as $person): ?>
                    <p class="title"><?= $person->name ?></p>
                <?php endforeach; ?>
            <?php endif; ?>
            <?php if (isset($item->particulars) && isset($item->particulars->description)): ?>
                <h2 class="title">Краткое описание: </h2>
                <p class="title"><?= $item->particulars->description ?></p>
            <?php elseif (isset($item->charge_code)): ?>
                <h2 class="title">Краткое описание: </h2>
                <p class="title">Contains fixed charge…</p>
            <?php endif; ?>
        </div>
    <?php endforeach; ?>
</div>