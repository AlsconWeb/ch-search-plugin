<body id="page-container">

<header role="banner">
    <img src="https://offshore.su/blog/wp-content/themes/article-directory/i/logo-offshore.png" alt="">

    <h1 class="heading-xlarge"><?= $company->company_name ?><br/>
        <?= $company->company_number ?></h1>
    <p class="heading-small">Дата основания: <?= formattingDate($company->date_of_creation) ?></p>
    <p class="heading-small">"Записки об офшорах" не проверяют достоверность представленной информации</p>
</header>
<main role="main">

    <h2 class="heading-large">Обзор</h2>
    <ul class="list list-bullet">

        <li>


            Юридический адрес:


            <?php
            $addressInfo = [
                $company->registered_office_address->address_line_1 ?? '',
                $company->registered_office_address->address_line_2 ?? '',
                $company->registered_office_address->locality ?? '',
                $company->registered_office_address->region ?? '',
                $company->registered_office_address->country ?? '',
                $company->registered_office_address->care_of ?? '',
                $company->registered_office_address->premises ?? '',
                $company->registered_office_address->po_box ?? '',
                $company->registered_office_address->postal_code ?? ''
            ] ?>

            <?= implode(', ', array_filter($addressInfo)) ?>

        </li>


        <li>Тип компании: <?= str_replace("-", " ", $company->type) ?></li>

        <?php if ($company->company_status) : ?>
            <li>Статус компании: <?= $company->company_status ?>
            </li>
        <?php endif; ?>

    </ul>


    <?php if ($company->accounts->next_made_up_to
        && $company->accounts->next_due
    ) : ?>
        <h3 class="heading-small">Финансовый отчет</h3>
        <ul class="list list-bullet">

            <li>Следующий отчет по
                <?= formattingDate($company->accounts->next_made_up_to) ?>
                сдать до
                <?= formattingDate($company->accounts->next_due) ?></li>


            <li>Последняя дата сдачи финансового отчета
                <?= formattingDate($company->accounts->last_accounts->made_up_to) ?></li>

        </ul>
    <?php endif; ?>
    <?php if (isset($company->confirmation_statement)) : ?>
        <?php if (true === $company->confirmation_statement->overdue) : ?>
            <h3 class="heading-small">Ведомость <span
                        class="overdue">ПРОСРОЧЕННАЯ</span></h3>
            <ul class="list list-bullet">
                <li>Первая дата ведомости
                    <?= formattingDate($company->confirmation_statement->next_made_up_to) ?>
                    сдать до
                    <?= formattingDate($company->confirmation_statement->next_due) ?>
                </li>
            </ul>
        <?php else : ?>
            <h3 class="heading-small">Ведомость </h3>
            <ul class="list list-bullet">
                <li><?= isset
                    (
                        $company->confirmation_statement->last_made_up_to
                    ) ? 'Следующая' : 'Первая' ?>
                    дата ведомости
                    <?= formattingDate($company->confirmation_statement->next_made_up_to) ?>
                    сдать до
                    <?= formattingDate($company->confirmation_statement->next_due) ?>
                </li>
            </ul>
        <?php endif; ?>

        <?php if (isset($company->confirmation_statement->last_made_up_to)) : ?>
            <ul class="list list-bullet">
                <li class="title">Последняя дата сдачи ведомости
                    <?= formattingDate($company->confirmation_statement->last_made_up_to) ?>
                </li>
            </ul>
        <?php endif; ?>
    <?php endif; ?>
    <?php if ($company->annual_return) : ?>
        <?php if ($company->annual_return->overdue) : ?>
            <h3 class="heading-small">Декларация <span
                        class="overdue">ПРОСРОЧЕННАЯ</span></h3>
        <?php else : ?>
            <?php if ($company->annual_return->next_made_up_to
                && $company->annual_return->next_due
            ) ?>
                <h3 class="heading-small">Декларация </h3>
            <?php if ($company->annual_return->next_made_up_to) : ?>
                <ul class="list list-bullet">
                    <li>Следующее декларирование с
                        <?= formattingDate($company->annual_return->next_made_up_to) ?>
                        по
                        <?= formattingDate($company->annual_return->next_due) ?>
                    </li>
                </ul>
            <?php endif; ?>
        <?php endif; ?>

        <?php if ($company->annual_return->last_made_up_to) : ?>
            <ul class="list list-bullet">
                <li>Последняя декларация была сдана
                    <?= formattingDate($company->annual_return->last_made_up_to) ?>
                </li>
            </ul>
        <?php endif; ?>
    <?php endif; ?>

    <?php if (isset($company->sic_codes)) : ?>
        <h3 class="heading-small">Вид деятельности (SIC) </h3>
        <ul class="list list-bullet">
            <?php foreach ($company->sic_codes as $code): ?>
                <li><?= searchDataByCode($code) ?> (<?= $code ?>)</li>
            <?php endforeach; ?>
        </ul>
    <?php endif; ?>

    <h2 class="heading-large">People</h2>

    <h2 class="heading-medium">Officers:<br>
        <?= $companyOfficers->total_results ?> должностных лиц /
        <?= $companyOfficers->resigned_count ?> в отставке
    </h2>


    <?php foreach ($companyOfficers->items as $item) : ?>
        <ul class="list list-bullet">
            <li><?= strtoupper(str_replace('-', ' ', $item->officer_role)) ?> <?= $item->name ?>

                <?php if (!$item->resigned_on): ?>
                    <span id="officer-status-tag-active" class="status-tag" style="color: green">ACTIVE</span>
                <?php else: ?>
                    <span id="officer-status-tag-active" class="status-tag" style="color: red">RESIGNED</span>
                <?php endif; ?>
            </li>


            <li>Назначен: <?= formattingDate($item->appointed_on) ?></li>


            <li>Дата рождения: <?= $item->date_of_birth->month
                .'.'
                .$item->date_of_birth->year ?></li>
            <?php if ($item->nationality) : ?>
                <li>Национальность: <?= $item->nationality ?></li>
            <?php endif; ?>


            <li>Почтовый адрес:


                <?= $item->address->premises
                    ? $item->address->premises.', '
                    : '' ?>
                <?= $item->address->address_line_1
                    ? $item->address->address_line_1.', '
                    : '' ?>
                <?= $item->address->address_line_2
                    ? $item->address->address_line_2.', '
                    : '' ?>
                <?= $item->address->locality
                    ? $item->address->locality.', '
                    : '' ?>
                <?= $item->address->region
                    ? $item->address->region.', ' : '' ?>
                <?= $item->address->country
                    ? $item->address->country.', ' : '' ?>
                <?= $item->address->postal_code
                    ? $item->address->postal_code : '' ?>


            </li>

            <li>Страна проживания: <?= $item->country_of_residence ?></li>

            <?php if (isset($item->occupation)) : ?>
                <li>Профессия: <?= $item->occupation ?></li>
            <?php endif; ?>


        </ul>
        <hr>
    <?php endforeach; ?>

    <h2 class="heading-medium">Люди со значительным контролем:</h2>


    <h2 class="heading-medium">
        <?= $companyScOfficers->active_count
            ? $companyScOfficers->active_count : 0 ?> активных лиц контролирующих компанию /
        <?= $statements->active_count
            ? $statements->active_count : 0 ?> активных
        состояний
    </h2>

    <?php if ($companyScOfficers->items): ?>
        <?php foreach ($companyScOfficers->items as $item)
            : ?>
            <ul class="list list-bullet">

                <li><?= $item->name ?>

                    <?php if (!$item->ceased_on): ?>
                        <span id="pscs-status-tag-active"
                              class="status-tag" style="color: green">АКТИВНО</span>
                    <?php else: ?>
                        <span id="pscs-status-tag-active"
                              class="status-tag" style="color: red">ПРИОСТАНОВЛЕНО</span>
                    <?php endif; ?>
                </li>

                <?php if ($item->address) : ?>
                    <li>Почтовый адрес:


                        <?= $item->address->premises
                            ? $item->address->premises
                            .', ' : '' ?>
                        <?= $item->address->address_line_1
                            ? $item->address->address_line_1
                            .', ' : '' ?>
                        <?= $item->address->address_line_2
                            ? $item->address->address_line_2
                            .', ' : '' ?>
                        <?= $item->address->locality
                            ? $item->address->locality
                            .', ' : '' ?>
                        <?= $item->address->region
                            ? $item->address->region.', '
                            : '' ?>
                        <?= $item->address->country
                            ? $item->address->country.', '
                            : '' ?>
                        <?= $item->address->postal_code
                            ? $item->address->postal_code
                            : '' ?>


                    </li>
                <?php endif; ?>
                <?php if ($item->notified_on) : ?>
                    <li> Зарегистрирован: <?= formattingDate($item->notified_on) ?></li>
                <?php endif; ?>

                <li>Дата рождения: <?= $item->date_of_birth->month
                    .'.'
                    .$item->date_of_birth->year ?></li>

                <?php if ($item->appointed_on) : ?>
                    <li> Назначен: <?= formattingDate($item->appointed_on) ?></li>
                <?php endif; ?>
                <?php if ($item->ceased_on) : ?>
                    <li> Прекращено: <?= formattingDate($item->ceased_on) ?></li>
                <?php endif; ?>

                <?php if ($item->nationality) : ?>
                    <li>Национальность: <?= $item->nationality ?></li>
                <?php endif; ?>

                <?php if ($item->country_of_residence) : ?>
                    <li>Страна
                        проживания: <?= $item->country_of_residence ?></li>
                <?php endif; ?>

                <?php if (isset($item->occupation)) : ?>
                    <li>Профессия: <?= $item->occupation ?></li>
                <?php endif; ?>

            </ul>


            <h3 class="heading-small">Характер управления:</h3>

            <ul class="list list-bullet">
                <?php foreach (
                    $item->natures_of_control
                    as $value
                ): ?>
                    <li> <?= str_replace(
                            '-',
                            ' ',
                            $value
                        ) ?></li>
                <?php endforeach; ?>
            </ul>

            </li>
            <hr>
        <?php endforeach; ?>
    <?php endif; ?>
    <?php if ($companyHistory && isset($companyHistory->items)) : ?>
        <h3 class="heading-large">Документы</h3>
        <table>
            <caption class="visually-hidden">Документы</caption>

            <thead>
            <tr>
                <th scope="col">Дата</th>
                <th scope="col">Форма</th>
                <th scope="col">Описание</th>
            </tr>
            </thead>
            <tbody>
            <!--            --><?php //if (isset($_COOKIE['dev'])) {
            //                var_dump($companyHistory);
            //                die;
            //            } ?>

            <?php foreach ($companyHistory->items as $item) : ?>
                <tr>
                    <td nowrap="nowrap"><?= formattingDate($item->date) ?></td>
                    <td scope="row"><?= $item->type ?></td>
                    <?php if ($item->description_values) :
                        switch (true) {
                            case $item->description_values->made_up_date:
                                $desc_details = ' made up to '.formattingDate($item->description_values->made_up_date);
                                break;
                            case $item->description_values->change_date && $item->description_values->officer_name:
                                $desc_details = ' for '.$item->description_values->officer_name.' on '.formattingDate(
                                        $item->description_values->change_date
                                    );
                                break;
                            case $item->description_values->appointment_date && $item->description_values->officer_name:
                                $desc_details = ' of '.$item->description_values->officer_name.' on '.formattingDate(
                                        $item->description_values->appointment_date
                                    );
                                break;
                            case $item->description_values->officer_name:
                                $desc_details = ' of '.$item->description_values->officer_name.' as a member '.(isset($item->description_values->termination_date) ? ('on '.formattingDate(
                                            $item->description_values->termination_date
                                        )) : '');
                                break;
                            case $item->description_values->old_address:
                                $desc_details = '<br> '.$item->description_values->old_address.' to '.$item->description_values->new_address.' on '.formattingDate(
                                        $item->description_values->change_date
                                    );
                                break;
                            case $item->description_values->default_address:
                                $desc_details = $item->description_values->default_address.' on '.formattingDate(
                                        $item->description_values->change_date
                                    );
                                break;
                        }
                        ?>
                        <?php if ($item->description_values->description) : ?>
                        <td><?= $item->description_values->description ?>
                            <?= $desc_details ?>
                        </td>
                    <?php else :
                        $item->description_values = (array)$item->description_values; ?>
                        <td><?= ucfirst(
                                str_replace(
                                    ['-', 'a person'],
                                    [' ', end($item->description_values).' as a person'],
                                    $item->description
                                )
                            ).' on '.formattingDate(reset($item->description_values)) ?>
                            <?= $desc_details ?>
                        </td>
                    <?php endif; ?>
                    <?php else : ?>
                    <td><?= str_replace('-', ' ', $item->description) ?>
                        <?php endif; ?>
                        <?php if (isset($item->associated_filings)): ?>


                            <?php foreach ($item->associated_filings as $associatedFiling):
                                ?>
                                <ul class="list list-bullet">
                                    <li class="document-description"><?= str_replace(
                                            '-',
                                            ' ',
                                            $associatedFiling->description
                                        ).' '
                                        .(isset($associatedFiling->description_values) && $associatedFiling->description_values->date ? ' on ' . formattingDate(
                                            $associatedFiling->description_values->date
                                        ) : '') ?>
                                        <?php if (isset($associatedFiling->description_values->capital)): ?>
                                            <?php foreach ($associatedFiling->description_values->capital as $capitalData): ?>
                                                <?= $capitalData->figure.' '.$capitalData->currency ?>
                                            <?php endforeach; ?>
                                        <?php endif; ?>
                                    </li>
                                </ul>
                            <?php
                            endforeach; ?>
                        <?php endif; ?>
                    </td>
                </tr>
            <?php endforeach; ?>

            </tbody>
        </table>
    <?php endif; ?>

</main>
</body>
</html>