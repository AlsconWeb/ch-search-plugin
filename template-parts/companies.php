<?php
/**
 * Created by PhpStorm.
 * User: bredevil
 * Date: 03.11.2018
 * Time: 15:20
 */

$data = getResults($_GET['val'], (isset($_GET['ch-page']) ? $_GET['ch-page'] - 1 : 0));

if ($data->total_results > 0) : ?>

    <div class="search-results-list">
        <h2>Результаты поиска</h2>
        <?php $pages = floor($data->total_results / 20) ?>
        <h3>Найдено: <?= $data->total_results ?> </h3>

        <ul>
            <?php
            $i = -1;
            foreach ($data->items as $item) :
                ++$i; ?>
                <li <?= ($i == 0 && !isset($_GET['ch-page'])) ? "class=first-item" : '' ?>>
                    <a href="/blog/company/<?= $item->company_number ?>"
                       class='search-result-item'><?= $item->title ?></a>
                    <?php if (isset($item->description)) : ?>
                        <p class="title"><?= $item->description ?></p>
                    <?php endif; ?>
                    <?php if (isset($item->address_snippet)) : ?>
                        <p class="title"><?= $item->address_snippet ?></p>
                    <?php endif; ?>
                    <?php if ($item->snippet) : ?>
                        <p class="title">Предидущее имя компании: <?= $item->snippet ?></p>
                    <?php endif; ?>
                </li>

            <?php endforeach; ?>
        </ul>

        <!--            pagination (start) -->
        <?php if ((int)$data->total_results > 20) : ?>
            <ul class="pagination">

                <?php if (isset($_GET['ch-page']) && $_GET['ch-page'] > 1) : ?>
                    <li class="arrow-left"><a
                                href="<?= '?val=' . $_GET['val'] . '&ch-page=' . ((int)$_GET['ch-page'] - 1) ?>"></a>
                    </li>
                <?php endif; ?>

                <?php $page = (isset($_GET['ch-page']) && (int)$_GET['ch-page'] > 1) ? $_GET['ch-page'] : 1; ?>
                <?php $page = ($page % 8 == 0) ? $page : (($page - $page % 8) + 1); ?>

                <?php for ($i = $page; $i <= ($page + 8); $i++) : ?>
                    <?php if (($i * 20) <= (int)$data->total_results - 20) : ?>
                        <?php $current_page = isset($_GET['ch-page']) ? $_GET['ch-page'] : 1; ?>
                        <li class="<?= ($current_page == $i) ? 'active' : '' ?>"><a
                                    href="<?= '?val=' . $_GET['val'] . '&ch-page=' . $i ?>"><?= $i ?></a>
                        </li>
                    <?php endif; ?>
                <?php endfor; ?>

                <?php if ((((int)($_GET['ch-page'] ?? 0) + 8) * 20) <= (int)$data->total_results - 20) : ?>
                    <li class="arrow-right"><a
                                href="<?= '?val=' . $_GET['val'] . '&ch-page=' . ((int)$_GET['ch-page'] + 1) ?>"></a>
                    </li>
                <?php endif; ?>

            </ul>

            <div class="col-xs-12">
                <form class="form-horizontal pages" method="get" action="#">
                    <input type="hidden" name="val" class="form-control"
                           value="<?= isset($_GET['val']) ? $_GET['val'] : '' ?>">
                    <input type="search" name="ch-page" class="form-control"
                           placeholder="Введите номер страницы: (1 - <?= $pages ?>)">
                    <input type="submit" value="Перейти">
                </form>
            </div>
        <?php endif; ?>
    </div>
<?php else: ?>
    <h2 id="not-items-found">Отсутствует информация по Вашему запросу</h2>
<?php endif; ?>