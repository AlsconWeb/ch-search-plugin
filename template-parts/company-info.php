<div class="active" id="block">

    <?php if ($company->registered_office_address) : ?>
        <h4 class='title'>Юридический адрес</h4>
        <p class="title">
            <?php

            $addressInfo = [
                $company->registered_office_address->address_line_1 ?? '',
                $company->registered_office_address->address_line_2 ?? '',
                $company->registered_office_address->locality ?? '',
                $company->registered_office_address->region ?? '',
                $company->registered_office_address->country ?? '',
                $company->registered_office_address->care_of ?? '',
                $company->registered_office_address->premises ?? '',
                $company->registered_office_address->po_box ?? '',
                $company->registered_office_address->postal_code ?? ''
            ] ?>

            <?= implode(', ', array_filter($addressInfo)) ?>
        </p>
    <?php endif; ?>

    <?php if ($company->company_status) : ?>
        <h4 class='title'>Статус компании</h4>
        <p class="company-title title">
            <?= $company->company_status ?>
            <?php if (isset($company->company_status_detail)): ?>
                <?= $company->company_status_detail ? " - ".str_replace(
                        "-",
                        " ",
                        $company->company_status_detail
                    ) : '' ?>
            <?php endif; ?>
        </p>
    <?php endif; ?>

    <?php if ($company->type) : ?>
        <h4 class='title'>Тип компании</h4>
        <p class="company-title title">
            <?= str_replace("-", " ", $company->type) ?>
        </p>
    <?php endif; ?>

    <?php if ($company->date_of_creation) : ?>
        <h4 class='title'>Дата основания</h4>
        <p class="company-title title">
<!--            --><?php //if(isset($_COOKIE['dev'])){
//                var_dump($company);die;
//            } ?>
            <?= formattingDate($company->date_of_creation) ?>
        </p>
    <?php endif; ?>

    <?php if ($company->date_of_cessation) : ?>
        <h4 class='title'>Дата ликвидации</h4>
        <p class="company-title title">
            <?= formattingDate($company->date_of_cessation) ?>
        </p>
    <?php endif; ?>

    <?php if ($company->accounts) : ?>


        <?php if (true === $company->accounts->overdue) : ?>
            <h4 class="title">Финансовый отчеты <span class="overdue">ПРОСРОЧЕННЫЕ</span>
            </h4>
        <?php elseif (isset($company->accounts->last_accounts->made_up_to)
            || (isset($company->accounts->next_made_up_to)
                && isset($company->accounts->next_due)
            )
        ) : ?>
            <h4 class="title">Финансовый отчет </h4>

        <?php endif; ?>

        <?php if (isset($company->accounts->next_made_up_to)
            && isset($company->accounts->next_due)
        ) : ?>
            <p class="title">Следующий отчет по
                <strong><?= formattingDate($company->accounts->next_made_up_to) ?></strong>
                сдать до
                <strong><?= formattingDate($company->accounts->next_due) ?></strong>
            </p>
        <?php endif; ?>

        <?php if (isset($company->accounts->last_accounts->made_up_to)) : ?>
            <p class="title">Последняя дата сдачи финансового отчета
                <strong><?= formattingDate($company->accounts->last_accounts->made_up_to) ?></strong>
            </p>
        <?php endif; ?>
    <?php endif; ?>

    <?php if (isset($company->confirmation_statement)) : ?>
        <?php if (true === $company->confirmation_statement->overdue) : ?>
            <h4 class="title">Ведомость <span
                        class="overdue">ПРОСРОЧЕННАЯ</span></h4>
            <p class="title">Первая дата ведомости
                <strong><?= formattingDate($company->confirmation_statement->next_made_up_to) ?></strong>
                сдать до
                <strong><?= formattingDate($company->confirmation_statement->next_due) ?></strong>
            </p>
        <?php else : ?>
            <h4 class="title">Ведомость </h4>
            <p class="title"><?= isset
                (
                    $company->confirmation_statement->last_made_up_to
                ) ? 'Следующая' : 'Первая' ?>
                дата ведомости
                <strong><?= formattingDate($company->confirmation_statement->next_made_up_to) ?></strong>
                сдать до
                <strong><?= formattingDate($company->confirmation_statement->next_due) ?></strong>
            </p>
        <?php endif; ?>

        <?php if (isset($company->confirmation_statement->last_made_up_to)) : ?>
            <p class="title">Последняя дата сдачи ведомости
                <strong><?= formattingDate($company->confirmation_statement->last_made_up_to) ?></strong>
            </p>
        <?php endif; ?>
    <?php endif; ?>

    <?php if (isset($company->annual_return)) : ?>
        <?php if (isset($company->annual_return->overdue)) : ?>
            <h4 class="title">Декларация <span
                        class="overdue">ПРОСРОЧЕННАЯ</span></h4>
        <?php else : ?>
            <?php if (isset($company->annual_return->next_made_up_to)
                && isset($company->annual_return->next_due)
            ) ?>
                <h4 class="title">Декларация </h4>
            <?php if (isset($company->annual_return->next_made_up_to)) : ?>
                <p class="title">Следующее декларирование с
                    <strong><?= formattingDate($company->annual_return->next_made_up_to) ?></strong>
                    по
                    <strong><?= formattingDate($company->annual_return->next_due) ?></strong>
                </p>
            <?php endif; ?>
        <?php endif; ?>

        <?php if (isset($company->annual_return->last_made_up_to)) : ?>
            <p class="title">Последняя декларация была сдана
                <strong><?= formattingDate($company->annual_return->last_made_up_to) ?></strong>
            </p>
        <?php endif; ?>
    <?php endif; ?>

    <?php if (isset($company->sic_codes)) : ?>
        <h4 class="title">Вид деятельности (SIC)</h4>
        <?php foreach ($company->sic_codes as $code): ?>
            <p class="title"><?= searchDataByCode($code) ?> (<?= $code ?>)</p>
        <?php endforeach; ?>
    <?php endif; ?>

    <?php if (isset($company->previous_company_names)) : ?>
        <h4 class="title">Предидущее имя компании </h4>
        <?php foreach ($company->previous_company_names as $company_name) : ?>
            <p class="title"><?= $company_name->name.' ('
                .$company_name->effective_from.' - '
                .$company_name->ceased_on.') ' ?></p>
        <?php endforeach; ?>
    <?php endif; ?>

</div>