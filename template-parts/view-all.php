<div class="active" id="block">
    <div class="tr">
        <?php if (is_user_logged_in()): ?>
            <div class="button-wrapper">
                <a href='<?= (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]" ?>?download'
                   class='button'>Скачать всю информацию</a>
            </div>
        <?php else: ?>
            <p>Эта опция доступна только для авторизованных пользователей</p>
            <div class="button-wrapper">
                <a href='/blog/wp-login.php'
                   class='button'>Авторизация</a>
            </div>
        <?php endif; ?>
    </div>
</div>
<?php generatePdf() ?>