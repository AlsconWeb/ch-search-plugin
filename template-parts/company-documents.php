<div id="block-2">
    <div class="tr">
        <div class="th">
            <p><strong>Дата</strong></p>
        </div>
        <div class="th">
            <p><strong>Описание</strong></p>
        </div>
        <div class="th">
            <p><strong>Просмотреть</strong></p>
        </div>
    </div>

    <?php if (isset($company_history) && isset($company_history->items)) : ?>
        <?php foreach ($company_history->items as $item) : ?>

            <div class="tr">
                <div class="td">
                    <p>
                        <?= formattingDate($item->date) ?>
                    </p>
                </div>
                <div class="td">
                    <?php $desc_details = ''; ?>

                    <?php if (isset($item->description_values)) :
                        switch (true) {
                            case $item->description_values->made_up_date:
                                $desc_details = ' made up to ' . formattingDate($item->description_values->made_up_date);
                                break;
                            case $item->description_values->change_date && $item->description_values->officer_name:
                                $desc_details = ' for ' . $item->description_values->officer_name . ' on ' . formattingDate($item->description_values->change_date);
                                break;
                            case $item->description_values->appointment_date && $item->description_values->officer_name:
                                $desc_details = ' of ' . $item->description_values->officer_name . ' on ' . formattingDate($item->description_values->appointment_date);
                                break;
                            case $item->description_values->officer_name:
                                $desc_details = ' of ' . $item->description_values->officer_name . ' as a member ' . (isset($item->description_values->termination_date) ? ('on ' . formattingDate($item->description_values->termination_date)) : '');
                                break;
                            case $item->description_values->old_address:
                                $desc_details = '<br> ' . $item->description_values->old_address . ' to ' . $item->description_values->new_address . ' on ' . formattingDate($item->description_values->change_date);
                                break;
                            case $item->description_values->default_address:
                                $desc_details = $item->description_values->default_address . ' on ' . formattingDate($item->description_values->change_date);
                                break;
                        }
                        ?>
                        <?php if (isset($item->description_values->description)) : ?>
                        <p><span
                                    class="document-description"><?= $item->description_values->description ?></span>
                            <?= $desc_details ?>
                        </p>
                        <?php else :
                        $item->description_values = (array)$item->description_values; ?>
                        <p><span
                                    class="document-description"><?= ucfirst(str_replace(['-', 'a person'],
                                    [' ', end($item->description_values) . ' as a person'],
                                    $item->description)) . ' on ' . formattingDate(reset($item->description_values)) ?></span>
                            <?= $desc_details ?>
                        </p>
                    <?php endif; ?>
                    <?php else : ?>
                        <p><span
                                    class="document-description"><?= str_replace('-', ' ', $item->description) ?></span>
                        </p>
                    <?php endif; ?>

                    <?php if (isset($item->associated_filings)): ?>
                        <ul>
                            <?php foreach ($item->associated_filings as $associatedFiling):
                                ?>
                                <li class="document-description"><?= str_replace('-', ' ',
                                        $associatedFiling->description) . ' '
                                    . (isset($associatedFiling->description_values) && $associatedFiling->description_values->date ? formattingDate($associatedFiling->description_values->date) : '') ?></li>
                            <?php
                            endforeach; ?>
                        </ul>
                    <?php endif; ?>
                </div>
                <div class="td">
                    <p class='download-pdf-now'>
                        <a href="https://beta.companieshouse.gov.uk<?= $item->links->self ?>/document?format=pdf&download=0"
                           target="_blank" download>открыть PDF</a>( <?= $item->pages ?> pages)</p>
                    <?php if ($item->type == 'AA'): ?>
                        <p class='download-pdf-now'>
                            <a href="https://beta.companieshouse.gov.uk<?= $item->links->self ?>/document?format=xhtml&download=1"
                               target="_blank" download>открыть iXBRL</a></p>
                    <?php endif; ?>
                </div>
            </div>
        <?php endforeach; ?>
    <?php else: ?>
        <h2 id="not-items-found">Отсутствует информация по Вашему запросу</h2>
    <?php endif; ?>
</div>