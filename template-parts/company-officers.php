<div id="block-3">
    <div class='officers'>
        <ul class="buttons nav nav-tabs">
            <?= showMenuOfficers($companyId, $companyType) ?>
        </ul>
        <div class="tab-content">
            <?php
            switch ($companyType) {
                case 'officers':
                    include(plugin_dir_path(__FILE__)
                        . 'officers/persons.php');
                    break;
                case 'persons-with-significant-control-statements':
                    include(plugin_dir_path(__FILE__)
                        . 'officers/statements.php');
                    break;
            }
            ?>
        </div>
    </div>
</div>