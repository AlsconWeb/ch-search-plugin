<?php

use Mpdf\HTMLParserMode;
use Mpdf\Mpdf;
use Mpdf\Output\Destination;

/**
 * @param     $val
 * @param int $index
 *
 * @return mixed
 */

function getResults($val, $index = 0)
{
    $search_param = str_replace(' ', '+', trim($val));
    $curl = curl_init();
    curl_setopt($curl, CURLOPT_USERPWD, get_option('ch_search_api_key'));
    curl_setopt(
        $curl,
        CURLOPT_URL,
        'https://api.companieshouse.gov.uk/search/companies?q='.$search_param
        .'&&start_index='.(((int)trim($index)) * 20)
        .'&&items_per_page=20'
    );
    curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'GET');
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl, CURLOPT_HEADER, false);
    curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
    $response = curl_exec($curl);
    if ($errno = curl_errno($curl)) {
        $error_message = curl_strerror($errno);
        echo "cURL error ({$errno}):\n {$error_message}";
    }
    curl_close($curl);
    return json_decode($response);
}

function getDetails($url)
{
    $curl = curl_init();
    curl_setopt($curl, CURLOPT_USERPWD, get_option('ch_search_api_key'));
    curl_setopt($curl, CURLOPT_URL, 'https://api.companieshouse.gov.uk'.$url);
    curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'GET');
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl, CURLOPT_HEADER, false);
    curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
    $response = curl_exec($curl);
    if ($errno = curl_errno($curl)) {
        $error_message = curl_strerror($errno);
        echo "cURL error ({$errno}):\n {$error_message}";
    }
    curl_close($curl);

    return json_decode($response);
}

function countActiveOfficers($companyOfficers)
{
    $result = 0;
    foreach ($companyOfficers as $officer) {
        if (!$officer->ceased_on) {
            ++$result;
        }
    }
    return $result;
}

function fillCompaniesHistoryTable()
{
    global $wpdb;

    $companies = $wpdb->get_results(
        "SELECT s.company_id        
            FROM wp_subscribe_data s 
            GROUP BY s.company_id"
    );
    foreach ($companies as $company) {
        $download_links = $wpdb->get_results(
            "SELECT download_link
             FROM companies_history 
            WHERE company_id = '".$company->company_id."'"
        );

        $company_documents = searchCompanyDataById(
            $company->company_id,
            'filing-history'
        );

        $current_documents = [];
        if ($download_links) {
            foreach ($download_links as $link) {
                array_push($current_documents, $link->download_link);
            }
        }
        $documents = [];
        if (count($company_documents->items) > 0) {
            if (isset($company_documents->items)) {
                foreach ($company_documents->items as $document) {
                    $fields = [];
                    $fields['company_id'] = $company->company_id;
                    $fields['download_link']
                        = "https://beta.companieshouse.gov.uk"
                        .$document->links->self
                        ."/document?format=pdf&download=0";
                    $fields['description'] = '';
                    if ($document->description_values) {
                        if ($document->description_values->made_up_date) {
                            $field = ' made up to '
                                .formattingDate($document->description_values->made_up_date);
                        } elseif ($document->description_values->change_date
                            && $document->description_values->officer_name
                        ) {
                            $field = ' for '
                                .$document->description_values->officer_name
                                .' on '
                                .formattingDate($document->description_values->change_date);
                        } elseif ($document->description_values->officer_name) {
                            $field = ' of '
                                .$document->description_values->officer_name
                                .' as a member ';
                        }
                        $fields['description'] .= $field.' ';
                    }

                    if ($document->description) {
                        $fields['description'] .= str_replace(
                                '-',
                                ' ',
                                $document->description
                            ).' '.($document->date
                                ? formattingDate($document->date) : '').' ';
                    }
                    if (isset($document->associated_filings)):
                        foreach (
                            $document->associated_filings as $associatedFiling
                        ):
                            $fields['description'] .= $associatedFiling->type
                                .' - '.str_replace(
                                    '-',
                                    ' ',
                                    $associatedFiling->description
                                );
                        endforeach;
                    endif;
                    $fields['date'] = $document->date;
                    if (!in_array(
                        $fields['download_link'],
                        $current_documents
                    )
                    ) {
                        array_push($documents, $fields);
                        $wpdb->insert('companies_history', $fields, '');
                    }
                }
            }
        }

        if (count($documents) > 0) {
            $userData = $wpdb->get_results(
                "SELECT user_email, company_name
                            FROM `wp_subscribe_data` sd
                            INNER JOIN `wp_users` wu
                            ON sd.user_id = wu.ID
                            WHERE  sd.company_id = '".$company->company_id
                ."'"
            );
            $mail_documents = [];
            foreach ($documents as $document) {
                array_push($mail_documents, $document['description']);
            }
            foreach ($userData as $user) {
                $mail_subject = 'Новые данные о '.$user->company_name.'('
                    .$company->company_id.')';
                $mail_template = file_get_contents(
                    ABSPATH
                    .'mail-template.txt'
                );
                $message = strtr(
                    $mail_template,
                    [
                        '{company_number}' => $company->company_id,
                        '{company_name}' => $user->company_name,
                        '{documents}' => implode("\r\n", $mail_documents),
                    ]
                );
                wp_mail($user->user_email, $mail_subject, $message);
            }
        }
    }
}

/**
 * @param        $id
 * @param string $type
 * filing-history, officers, persons-with-significant-control
 *
 * @return mixed
 */
function searchCompanyDataById($id, $type = '')
{
    $curl = curl_init();
    curl_setopt($curl, CURLOPT_USERPWD, get_option('ch_search_api_key'));
    curl_setopt(
        $curl,
        CURLOPT_URL,
        'https://api.companieshouse.gov.uk/company/'.$id.'/'.$type
    );
    curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'GET');
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl, CURLOPT_HEADER, false);
    curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
    $response = curl_exec($curl);
    if ($errno = curl_errno($curl)) {
        $error_message = curl_strerror($errno);
        echo "cURL error ({$errno}):\n {$error_message}";
    }
    curl_close($curl);
    return json_decode($response);
}

/**
 * @param $date
 *
 * @return string
 */
function formattingDate($date)
{
    if ($date) {
        return implode('.', array_reverse(explode('-', $date)));
    }
}

function translateChargeStatus($statusSlug)
{
    $statuses = [
        'outstanding' => 'Не исполнено',
        'satisfied' => 'Выполнено',
        'fully-satisfied' => 'Полностью выполнено',
        'part-satisfied' => 'Частично выполнено'
    ];

    if (!isset($statuses[$statusSlug])) {
        return '';
    }

    return $statuses[$statusSlug];
}

function translateParticularsType($statusSlug)
{
    $statuses = [
        'short-particulars' => 'Short particulars',
        'charged-property-description' => 'Charged property description',
        'charged-property-or-undertaking-description' => 'Charged property or undertaking description',
        'brief-description' => 'Brief description',
    ];

    if (!isset($statuses[$statusSlug])) {
        return '';
    }

    return $statuses[$statusSlug];
}

function translateAmountSecuredType($statusSlug)
{
    $statuses = [
        'amount-secured' => 'Amount secured',
        'obligations-secured' => 'Obligations secured'
    ];

    if (!isset($statuses[$statusSlug])) {
        return '';
    }

    return $statuses[$statusSlug];
}

function prepareChargeRegistrationDocumentLink($transactions)
{
    if (!isset($transactions) || empty($transactions)) {
        return '';
    }
    $search = 'create-charge';
    foreach ($transactions as $key => $value) {
        if (preg_match("/{$search}/i", $value->filing_type)) {
            unset($transactions[$key]);

            return $value->links->filing;
        }
    }
    return '';
}

function prepareTitle($title)
{
    return ucfirst(
        str_replace(
            [
                '_',
                '-'
            ],
            ' ',
            $title
        )
    );
}

/**
 * @param $code_sic
 *
 * @return mixed
 */
function searchDataByCode($code_sic)
{
    $handle = fopen(__DIR__."/SIC07.csv", "r");
    while (($data = fgetcsv($handle, 2000, ',')) !== false) {
        if ((int)$data[0] == (int)$code_sic) {
            return $data[1];
            fclose($handle);
        }
    }
}

add_action('wp_ajax_subscribeCompany', 'subscribeCompany');
add_action('wp_ajax_nopriv_subscribeCompany', 'subscribeCompany');
function subscribeCompany()
{
    global $wpdb;
    $fields['ip'] = $_SERVER['REMOTE_ADDR'];
    $fields['company_id'] = $_POST['company_id'];
    $fields['company_name'] = $_POST['company_name'];
    if (is_user_logged_in()) {
        $fields['user_id'] = wp_get_current_user()->ID;
    }
    $wpdb->delete(
        'wp_subscribe_data',
        [
            'ip' => $_SERVER['REMOTE_ADDR'],
            'user_id' => isset($fields['user_id']) ? $fields['user_id'] : 'NULL',
            'company_id' => $_POST['company_id'],
        ]
    );
    $wpdb->insert('wp_subscribe_data', $fields, '');
    $company_documents = searchCompanyDataById(
        $_POST['company_id'],
        'filing-history'
    );
    if (count($company_documents->items) > 0) {
        if (isset($company_documents->items)) {
            foreach ($company_documents->items as $document) {
                $fields = [];
                $fields['company_id'] = $_POST['company_id'];
                $fields['download_link'] = "https://beta.companieshouse.gov.uk"
                    .$document->links->self
                    ."/document?format=pdf&download=0";
                if ($document->description_values) {
                    if ($document->description_values->made_up_date) {
                        $field = ' made up to '
                            .formattingDate($document->description_values->made_up_date);
                    } elseif ($document->description_values->change_date
                        && $document->description_values->officer_name
                    ) {
                        $field = ' for '
                            .$document->description_values->officer_name
                            .' on '
                            .formattingDate($document->description_values->change_date);
                    } elseif ($document->description_values->officer_name) {
                        $field = ' of '
                            .$document->description_values->officer_name
                            .' as a member ';
                    }
                    $fields['description'] = $field;
                } else {
                    $fields['description'] = $document->description;
                }
                $fields['date'] = $document->date;
                $wpdb->insert('companies_history', $fields, '');
            }
        }
    }
    if (!is_user_logged_in()) {
        wp_send_json(
            [
                'redirect_url' => wp_login_url(),
            ]
        );
    }
}

add_action('wp_ajax_unSubscribeCompany', 'unSubscribeCompany');
add_action('wp_ajax_nopriv_unSubscribeCompany', 'unSubscribeCompany');
function unSubscribeCompany()
{
    global $wpdb;
    $fields['company_id'] = $_POST['company_id'];
    $fields['user_id'] = wp_get_current_user()->ID;
    $wpdb->delete('wp_subscribe_data', $fields);
}


function saveSessionData()
{
    global $wpdb;
    $fields['user_id'] = wp_get_current_user()->ID;
    $where['ip'] = $_SERVER['REMOTE_ADDR'];
    $where['user_id'] = 'NULL';
    $wpdb->update('wp_subscribe_data', $fields, $where);
}

add_action('wp_login', 'saveSessionData');

function checkUserSubscribe($company_id)
{
    global $wpdb;
    $userData = $wpdb->get_results(
        "SELECT COUNT(id) as amount FROM `wp_subscribe_data` 
            WHERE `user_id` = ".(int)wp_get_current_user()->ID." 
            AND `company_id` = '".$company_id."'
            ORDER BY `id` DESC"
    );
    return $userData[0]->amount > 0;
}

add_action('init', 'doRewrite');

function doRewrite()
{
    add_rewrite_rule(
        '^(company)/([^/]*)/?([^/]*)',
        'index.php?pagename=$matches[1]&company_id=$matches[2]&type=$matches[3]',
        'top'
    );
    add_filter(
        'query_vars',
        function ($vars) {
            $vars[] = 'company_id';
            $vars[] = 'type';
            return $vars;
        }
    );
}

/**
 * @param      $companyId
 * @param null $companyType
 *
 * @return string
 */
function showMenu($company, $companyType = null)
{
    $companyId = $company->company_number;
    $data = [
        [
            'url' => '/blog/company/'.$companyId,
            'title' => 'Обзор',
            'htmlClass' => 'search-result-item',
            'types' => [null],
        ],
        [
            'url' => '/blog/company/'.$companyId.'/filing-history',
            'title' => 'Документы',
            'htmlClass' => 'search-history-item',
            'types' => ['filing-history'],
        ],
        [
            'url' => '/blog/company/'.$companyId
                .'/officers',
            'title' => 'Люди',
            'htmlClass' => 'search-people-item',
            'types' => [
                'officers',
                'persons-with-significant-control-statements',
            ],
        ],
        [
            'url' => '/blog/company/'.$companyId
                .'/charges',
            'title' => 'Обременение',
            'htmlClass' => 'search-people-item',
            'types' => ['charges'],
        ],
        [
            'url' => '/blog/company/'.$companyId
                .'/view-all',
            'title' => 'Смотреть все',
            'htmlClass' => 'search-people-item',
            'types' => ['view-all'],
        ],
    ];
    $data = removeAdditionalItems($company, $data);
    $menu = [];
    foreach ($data as $item) {
        if (in_array($companyType, $item['types'])) {
            $menuItem = '<li class="active"><a href="'
                .$item['url'].'" class="'
                .$item['url'].'">'
                .$item['title'].'</a></li>';
        } else {
            $menuItem = '<li><a href="'
                .$item['url'].'" class="'
                .$item['htmlClass'].'">'
                .$item['title'].'</a></li>';
        }
        array_push(
            $menu,
            $menuItem
        );
    }

    return implode('', $menu);
}

function removeAdditionalItems($company, $menuItems)
{
    if (!$company->has_charges) {
        unset($menuItems[3]);
    }

    return $menuItems;
}

function prepareChargeDetailUrl($link)
{
    $slug = basename($link);
    return str_replace('/'.$slug, '?chargeSlug='.$slug, $link);
}

function generatePdf()
{
    if (isset($_GET['download'])) {
        ob_end_clean();
        $companyId = get_query_var('company_id');
        $mpdf = new Mpdf(['tempDir' => __DIR__.'/tmp']);
        $company = searchCompanyDataById($companyId);
        $mpdf->WriteHTML(
            includeWithVariables(
                plugin_dir_path(__FILE__).'../template-parts/export/export.php',
                [
                    'company' => $company,
                    'companyOfficers' => searchCompanyDataById($_GET['company_id'], 'officers'),
                    'companyScOfficers' => searchCompanyDataById(
                        $_GET['company_id'],
                        'persons-with-significant-control'
                    ),
                    'statements' => searchCompanyDataById(
                        $_GET['company_id'],
                        'persons-with-significant-control-statements'
                    ),
                    'companyHistory' => searchCompanyDataById($companyId, 'filing-history')
                ],
                false
            )
        );
        ob_clean();
        $mpdf->Output(str_replace(' ', '-', strtolower($company->company_name)).'.pdf', Destination::DOWNLOAD);
        die();
    }
}

/**
 * @param      $companyId
 * @param null $companyOfficerType
 *
 * @return string
 */
function showMenuOfficers($companyId, $companyOfficerType = null)
{
    $data = [
        [
            'url' => '/blog/company/'.$companyId.'/officers',
            'title' => 'Люди',
            'htmlClass' => 'search-result-item',
            'type' => 'officers',
        ],
        [
            'url' => '/blog/company/'.$companyId
                .'/persons-with-significant-control-statements',
            'title' => 'Люди со
                    значительным контролем',
            'htmlClass' => 'search-history-item',
            'type' => 'persons-with-significant-control-statements',
        ],
    ];
    $menu = [];
    foreach ($data as $item) {
        if ($item['type'] == $companyOfficerType) {
            $menuItem = '<li class="active"><a href="'
                .$item['url'].'" class="'
                .$item['url'].'">'
                .$item['title'].'</a></li>';
        } else {
            $menuItem = '<li><a href="'
                .$item['url'].'" class="'
                .$item['htmlClass'].'">'
                .$item['title'].'</a></li>';
        }
        array_push(
            $menu,
            $menuItem
        );
    }
    return implode('', $menu);
}

function includeWithVariables($filePath, $variables = array(), $print = true)
{
    $output = null;
    if (file_exists($filePath)) {
        extract($variables);
        ob_start();
        include $filePath;
        $output = ob_get_clean();
    }
    if ($print) {
        print $output;
    }
    return $output;
}
