jQuery(document).ready(function ($) {
   $('#filter-charges-input').on('change', function (e) {
       console.table(this.checked);
       if (this.checked) {
           $('.js-charge-item[data-status="satisfied"]').hide();
           $('.js-charge-item[data-status="fully-satisfied"]').hide();
       } else {
           $('.js-charge-item[data-status="satisfied"]').show();
           $('.js-charge-item[data-status="fully-satisfied"]').show();
       }
   })
    $('.js-subscribe-button').on('click', function (e) {
        e.preventDefault();

        $.ajax({
            url    : '/blog/wp-admin/admin-ajax.php',
            type   : 'POST',
            async  : false,
            data   : {
                'action'      : 'subscribeCompany',
                'company_id'  : $('.js-company-name').data('id'),
                'company_name': $('.js-company-name').text(),
            },
            success: function (response) {
                console.log(response);
                if (response != '' && typeof response.redirect_url != 'undefined') {
                    window.location.href = response.redirect_url;
                } else {
                    window.location.href = '/blog/menedzher-podpisok';
                }
            }
        })
    })

    if($('.js-officer-item').length > 0){
        $('.js-officers-total').text($('.js-officer-item').length);
        $('.js-officers-resigned').text($('.js-officer').length);
        $('.js-officers-current').text($('.js-officer-active').length);
    }

    $('#show-current').on('change', function () {
        switch ($(this).prop('checked')) {
            case true:
                $('.js-officers-current').closest('.title').show();
                $('.js-officers-total').closest('.title').hide();
                $('.js-officers-resigned').closest('.title').hide();
                $('.js-officer').hide();
                break;
            default:
                $('.js-officers-total').closest('.title').show();
                $('.js-officers-resigned').closest('.title').show();
                $('.js-officers-current').closest('.title').hide();
                $('.js-officer').show();
                break;
        }
    })

    $('.js-unsubscribe-button').on('click', function (e) {
        e.preventDefault();
        $.ajax({
            url    : '/blog/wp-admin/admin-ajax.php',
            type   : 'POST',
            async  : false,
            data   : {
                'action'    : 'unSubscribeCompany',
                'company_id': $('.js-company-name').data('id'),
            },
            success: function (response) {
                // window.location = document.referrer + '?index=1';
                window.location.href = '/blog/menedzher-podpisok';

            }
        })
    })
})
