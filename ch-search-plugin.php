<?php
/*
Plugin Name: Plugin for CompaniesHouseAPI
Version: 2.1
Description: Search Plugin for Companies House. Add the shortcode to the page content ([ch_search_form])
Author: Alscon (Khalimov Renato)
*/

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

require_once plugin_dir_path( __FILE__ ) . '/vendor/autoload.php';
include_once( dirname( __FILE__ ) . '/public/admin.php' );
include_once( dirname( __FILE__ ) . '/public/results.php' );

class ch_search_shortcode {
	static $add_script;
	static $add_css;

	static function init() {
		add_shortcode( 'ch_search_form', [ __CLASS__, 'ch_search_func' ] );
		add_action( 'wp_enqueue_scripts', [ __CLASS__, 'register_script' ] );
		add_action( 'wp_enqueue_scripts', [ __CLASS__, 'register_css' ] );
		add_action( 'wp_footer', [ __CLASS__, 'print_script' ] );
	}

	public function on_activation() {
		require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
		if ( file_exists( plugin_dir_path( __FILE__ ) . 'migrations/companies_history.sql' ) ) {
			dbDelta( file_get_contents( plugin_dir_path( __FILE__ ) . 'migrations/companies_history.sql' ) );
		}
	}


	static function ch_search_func() {
		self::$add_script = true;
		self::$add_css    = true;

		ob_start();
		include_once( 'main.php' );
		$out = ob_get_contents();
		ob_end_clean();

		return $out;
	}

	static function register_script() {
		wp_deregister_script( 'jquery' );
		wp_register_script( 'jquery', '//code.jquery.com/jquery-2.2.4.min.js' );
		wp_register_script( 'vue-js', plugins_url( 'public/js/vue.js', __FILE__ ) );
		wp_register_script( 'main-js', plugins_url( 'public/js/main.js', __FILE__ ) );
		wp_register_script( 'prefix_bootstrap', '//maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js' );
		wp_register_script( 'prefix_vue-resource',
			'//cdnjs.cloudflare.com/ajax/libs/vue-resource/1.0.2/vue-resource.js' );
		wp_localize_script( 'main-js', 'ch_ajax', [
			'ajax_url' => admin_url( 'admin-ajax.php' ),
		] );

	}

	static function register_css() {
		wp_register_style( 'prefix_bootstrap', '//maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css' );
		wp_register_style( 'ch_search', plugins_url( 'public/css/main.css', __FILE__ ) );
	}

	static function print_script() {
		if ( ! self::$add_script && ! self::$add_css ) {
			return;
		}

		//include js
		wp_print_scripts( 'vue-js' );
		wp_print_scripts( 'main-js' );
		wp_enqueue_script( 'ch-jquery' );
		wp_enqueue_script( 'prefix_bootstrap' );
		wp_enqueue_script( 'prefix_vue-resource' );

		//include css
//        wp_enqueue_style('prefix_bootstrap');
		wp_enqueue_style( 'ch_search' );
	}

}

ch_search_shortcode::init();

?>