<?php

    /**
     * Created by PhpStorm.
     * User: macbook
     * Date: 04.11.17
     * Time: 11:24
     */
    class OpenCorporates
    {
        static $add_script;
        static $add_css;

        static function init()
        {
            add_shortcode('oc_search', [__CLASS__, 'ch_search_func']);
            add_action('init', [__CLASS__, 'register_script']);
            add_action('init', [__CLASS__, 'register_css']);
            add_action('wp_footer', [__CLASS__, 'print_script']);
        }

        public function on_activation()
        {
            require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
            if (file_exists(plugin_dir_path(__FILE__) . 'migrations/companies_history.sql')) {
                dbDelta(file_get_contents(plugin_dir_path(__FILE__) . 'migrations/companies_history.sql'));
//                fillCompaniesHistoryTable();
            }
        }

        public function on_deactivation()
        {
            global $wpdb;
            $wpdb->query("DROP TABLE IF EXISTS `companies_history`");
        }

        static function ch_search_func($atts)
        {
            ob_start();
            self::$add_script = true;
            self::$add_css    = true;
            require_once('main.php');
            $out = ob_get_contents();
            ob_end_clean();
            return $out;
        }

        static function register_script()
        {
            wp_register_script('prefix_bootstrap', '//maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js');
        }

        static function register_css()
        {
            wp_register_style('prefix_bootstrap', '//maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css');
            wp_register_style('oc_search', plugins_url('public/css/main.css', __FILE__));
        }

        static function print_script()
        {
            if (!self::$add_script && !self::$add_css) {
                return;
            }

            wp_enqueue_script('prefix_bootstrap');
            wp_enqueue_style('oc_search');
        }
    }